import EchartInstance from '../src/components/EchartInstance/index.vue'
import Echart from '../src/components/Echart/index.vue'
import AuthBtn from '../src/components/AuthBtn/index.vue'
import Form from '../src/components/Form/index.vue'
import Page from '../src/components/Page/index.vue'
import Table from '../src/components/Table/index.vue'
// import GLTF from '../src/components/GLTF/index.vue'

const components = [EchartInstance, Echart, AuthBtn, Form, Page, Table]
components.forEach((item) => {
	item.install = function(Vue) {
		Vue.component(item.name, item)
	}
})

const install = function(Vue) {
	components.forEach(function(item) {
		if (item.install) {
			Vue.use(item)
		} else if (item.name) {
			Vue.component(item.name, item)
		}
	})
}

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
	install(window.Vue)
}
export { EchartInstance, Echart, AuthBtn, Form, Page, Table }
export default {
	version: '0.3.4',
	install,
}
