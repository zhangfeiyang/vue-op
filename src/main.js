import Vue from 'vue'
import App from './App'
import router from '@/router'
import store from '@/store'
import ElementUI from 'element-ui'
import VueI18n from 'vue-i18n'
import { quillEditor } from 'vue-quill-editor'
import axios from '@/http/axios'
import axiosDown from '@/http/download'
import '@/utils/exe'
import '@/styles/index.scss'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.component('Editor', quillEditor)
Vue.use(VueI18n)

Vue.prototype.$axios = axios
Vue.prototype.$axiosDown = axiosDown

const i18n = new VueI18n({
    locale: 'en',
    messages: {
        zh: require('@/lang/zh'),
        en: require('@/lang/en'),
    },
})

new Vue({
    router,
    store,
    i18n,
    render: (h) => h(App),
}).$mount('#app')
