import Vue from 'vue'
import Vuex from 'vuex'
import options from './modules/options'
import session from './modules/session'

Vue.use(Vuex)

const list = [
    { stateName: 'curRole', initVal: {} },
    { stateName: 'curSystem', initVal: {}, local: true },
]

const state = list.reduce((obj, { stateName, initVal, session, local }) => {
    let value
    try {
        if (session) value = JSON.parse(sessionStorage.getItem(stateName))
        if (local) value = JSON.parse(localStorage.getItem(stateName))
    } catch (error) {}
    obj[stateName] = value || initVal
    return obj
}, {})

const mutations = list.reduce((obj, { stateName, session, local }) => {
    obj[stateName] = (state, val) => {
        state[stateName] = val
        if (session) sessionStorage.setItem(stateName, JSON.stringify(val))
        if (local) localStorage.setItem(stateName, JSON.stringify(val))
    }
    return obj
}, {})

export default new Vuex.Store({
    state,
    mutations,
    actions: {},
    modules: { session, options },
})
