/** 文件说明
 * @info 此文件存储需保存在sessionStorage中的状态、刷新页面时还需要保留的状态
 * @param dispatch(dispatchName, config) 第二个参数config为一个对象
 * @param config.refresh 传true时则不取state的值，发请求获取结果并更新到state中
 * @param config.params 请求所带的参数
 */

import axios from '@/http/axios'

const list = [
    // 登录信息
    { stateName: 'loginInfo', api: (data) => axios.post(`/commonApi/login`, data), initVal: {} },
    // 权限树，渲染菜单与生成router用
    { stateName: 'menuList', api: (params) => axios.get(`/commonApi/system/page/tree`, { params }), initVal: [] },
]

const state = list.reduce((obj, { stateName, initVal }) => {
    let value
    try {
        value = JSON.parse(sessionStorage.getItem(stateName))
    } catch (error) {}
    obj[stateName] = value || initVal
    return obj
}, {})

const mutations = list.reduce((obj, { stateName }) => {
    obj[stateName] = (state, val) => {
        sessionStorage.setItem(stateName, JSON.stringify(val))
        state[stateName] = val
    }
    return obj
}, {})

// 判断是否为空或空数组或空对象
const isEmpty = (obj) => {
    if (Array.isArray(obj)) return !obj.length
    if (typeof obj === 'object') return !Object.keys(obj).length
    return !obj
}

const actions = list.reduce((obj, { stateName, api }) => {
    obj[stateName] = ({ commit }, config = {}) => {
        const { refresh = false, params = {} } = config
        if (!isEmpty(state[stateName]) && !refresh) {
            return Promise.resolve(state[stateName])
        }
        return api(params).then(({ data }) => {
            commit(stateName, data)
            return data
        })
    }
    return obj
}, {})

export default {
    namespaced: true,
    state,
    mutations,
    actions,
}
