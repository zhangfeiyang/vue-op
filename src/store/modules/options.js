/** 文件说明
 * @info 此文件存储各页面select框的下拉列表
 * @param list:Array [item] 添加新条目时只用在list列表中添加数据
 * @param item.stateName:String store的名字，对应的mutation与action名字也为stateName，方便通过页面文件全局查找
 * @param item.api:Function 获取store值的接口方法
 * @param dispatch(dispatchName, config) 第二个参数config为一个对象
 * @param config.refresh 传true时则不取state的值，发请求获取结果并更新到state中
 * @param config.params 请求所带的参数
 */

import axios from '@/http/axios'

const list = [{ stateName: 'xx', api: () => axios.get(`/commonApi/auth/xx`) }]

const state = list.reduce((obj, { stateName }) => {
    obj[stateName] = []
    return obj
}, {})

const mutations = list.reduce((obj, { stateName }) => {
    obj[stateName] = (state, list) => {
        state[stateName] = list
    }
    return obj
}, {})

// 判断是否为空或空数组或空对象
const isEmpty = (obj) => {
    if (Array.isArray(obj)) return !obj.length
    if (typeof obj === 'object') return !Object.keys(obj).length
    return !obj
}

// 防止单次渲染时间内的多次重复请求
const requestObj = {}
const actions = list.reduce((obj, { stateName, api, format }) => {
    obj[stateName] = ({ commit }, config = {}) => {
        const { refresh = false, params = {} } = config
        if (!isEmpty(state[stateName]) && !refresh) {
            return Promise.resolve(state[stateName])
        }
        if (!requestObj[stateName]) {
            requestObj[stateName] = () => api(params)
        }
        return requestObj[stateName]().then(({ data = [] }) => {
            setTimeout(() => {
                delete requestObj[stateName]
            }, 10)
            if (format) {
                data = format(data)
            } else {
                data.forEach((item) => {
                    item.label = item.title || item.name
                    item.value = item.id
                })
            }
            commit(stateName, data)
            return data
        })
    }
    return obj
}, {})

export default {
    namespaced: true,
    state,
    mutations,
    actions,
}
