/** 文件说明
 * @return 返回生成axios实例的方法，各实例可添加各自的默认值与拦截器
 * 公共的默认值与拦截器在此文件中设置
 * @param prefix:String 配合vue.config.js文件的proxy实现不重启服务切换代理，若不是空值请一定要加上isDev判断，保证在production环境上为空值
 */
import axios from 'axios'
import { Message } from 'element-ui'
import qs from 'qs'
import { getToken, setToken } from '@/utils'

const isDev = process.env.NODE_ENV === 'development'
// const prefix = isDev ? '' : '' // 本地环境
const prefix = isDev ? '/dev' : '' // 开发环境

export default () => {
    const service = axios.create({
        baseURL: prefix,
        timeout: 10000,
        paramsSerializer: function(params) {
            // 空参数过滤
            Object.entries(params).forEach(([key, value]) => {
                if (value === null || value === undefined || value === '') {
                    console.warn(`参数${key}的值为'${value}'，已自动过滤`)
                    delete params[key]
                }
            })
            return qs.stringify(params, { arrayFormat: 'comma' })
        },
    })

    // 发送前拦截
    service.interceptors.request.use((config) => {
        const { params = {}, responseType } = config

        // 设置token，axios.create中设置无效，因此时store还是undefined
        config.headers.authorization = getToken()

        // 导出接口添加时间戳防缓存
        if (responseType === 'blob') {
            const __t = new Date().getTime()
            config.params = { ...params, __t }
        }
        return config
    })

    // 接收后拦截
    service.interceptors.response.use(
        (response) => {
            // token设置
            if (response.headers.authorization) {
                setToken(response.headers.authorization)
            }
            const { status, data, statusText } = response
            if (status === 200) {
                // 返回文件流时
                if (response.headers['content-type'] === 'application/octet-stream') {
                    return response
                }
                return data
            }
            Message.error(statusText)
            return Promise.reject(data)
        },
        (error) => {
            Message.error(error.response.data?.msg || 'request error')
            if (error.response.status === 401) {
                sessionStorage.setItem('preRoute', window.location.pathname + window.location.search)
                window.location.pathname = '/login'
            }
            return Promise.reject(error.response.data)
        },
    )

    return service
}
