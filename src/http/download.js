import axiosCreater from './axiosCreater'
const service = axiosCreater()

service.defaults.responseType = 'blob'

// 接收后拦截
service.interceptors.response.use((response) => {
    // 方法一，使用FileReader，无需手动回收
    return new Promise((resolve) => {
        const reader = new window.FileReader()
        reader.readAsDataURL(response.data)
        reader.onload = function() {
            const disposition = response.headers['content-disposition']
            if (disposition) {
                const fileName = disposition.split("filename*=UTF-8''")[1]
                if (fileName) {
                    let a = document.createElement('a')
                    a.href = reader.result
                    a.download = decodeURIComponent(fileName)
                    a.click()
                    a = null
                }
            }
            resolve(reader.result)
        }
    })

    // 方法二，使用URL.createObjectURL，但需手动回收
    // const src = window.URL.createObjectURL(response.data)
    // return src
    // window.URL.revokeObjectURL(src) // 不可在此处回收，会导致找不到图片，报错net::ERR_FILE_NOT_FOUND
})

export default service
