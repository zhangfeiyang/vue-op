import axiosCreater from './axiosCreater'
import { Message } from 'element-ui'
const service = axiosCreater()

// 短时间重复请求沿用上次的promise
const requestList = { get: {}, post: {}, put: {}, delete: {} }
const { get, post, put, delete: del } = service
const list = [
	{ name: 'get', method: get },
	{ name: 'post', method: post },
	{ name: 'put', method: put },
	{ name: 'delete', method: del },
]
list.forEach(({ name, method }) => {
	service[name] = (url, ...args) => {
		const arg0 = args[0]
		let key
		if (['get', 'delete'].includes(name)) {
			key = url + (arg0 ? JSON.stringify(arg0.params) : '')
		} else {
			key = url + JSON.stringify(arg0)
		}
		const callback = requestList[name][key]
		if (callback) {
			console.warn(`${name}:${url}短时间重复请求，沿用上次的请求结果`)
			return callback
		}
		const result = method(url, ...args)
		requestList[name][key] = result
		result.finally(() => {
			delete requestList[name][key]
		})
		return result
	}
})

// 接收后拦截
service.interceptors.response.use((data) => {
	if (data.code === 0) {
		if (data.msg && data.msg !== 'success') Message.success(data.msg)
		if (data.data && Array.isArray(data.data)) data.list = data.data
		return data
	}
	Message.error(data.msg)
	return Promise.reject(data)
})

export default service
