import store from '@/store'
/** 获取vue组件可解构的属性
 *  @param item:Object
 */
export const getVueComAttrs = (item = {}) => {
    const result = { attrs: {}, on: {} }
    Object.entries(item).forEach(([key, value]) => {
        if (typeof value === 'function') {
            result.on[key] = value
        }
        result.attrs[key] = value
        result[key] = value
    })
    return result
}

/** 获取页面的按钮组信息
 *  @param path:Array 页面路由数组
 *  @param list:Array 菜单列表
 */
export const getPageBtns = (path, list = store.state.session.menuList) => {
    if (!path.length) return []
    const pageInfo = list.find((item) => item.path === path[0])
    if (pageInfo) {
        path.shift()
        const { btns = [], children = [] } = pageInfo
        if (path.length) {
            return getPageBtns(path, children)
        }
        return btns
    }
    return []
}

/** 通过pageId获取页面路由
 *  @param pageId:String | Number，余下2个参数不用传
 */
export const getPathByPageId = (pageId = '', list = store.state.session.menuList, parentPath = '') => {
    if (!pageId) return ''
    let result = ''
    const has = list.some((item) => {
        const { children = [], path, id } = item
        if (pageId == id) {
            result = `${parentPath}/${path}`
            return true
        }
        if (children.length) {
            const res = getPathByPageId(pageId, children, `${parentPath}/${path}`)
            if (res) result = res
            return res
        }
        return false
    })
    if (has) return result
    return false
}

/** 字符串转对象、数组、function
 *  @param str:String
 */
export const objParse = (str = '') => {
    if (!str) return ''
    if (str.includes('function') || str.includes('=>')) return eval(str)
    return JSON.parse(str, function(k, v) {
        return eval('(function(){return ' + v + ' })()')
    })
}

/** 获取token */
export const getToken = () => {
    return sessionStorage.getItem('authorization')
}

/** 设置token */
export const setToken = (authorization) => {
    sessionStorage.setItem('authorization', authorization)
}

/** 根据array、object、function、string得到最终字符串 */
export const getFinalValue = (data, value, row, index) => {
    if (!data && data !== 0) return ''
    if (typeof data === 'string') {
        return data
    } else if (typeof data === 'function') {
        return data(value, row, index)
    } else if (Array.isArray(data)) {
        return data.find((item) => item.value == value)?.label
    } else if (typeof data === 'object') {
        return data[value]
    }
    return data
}
