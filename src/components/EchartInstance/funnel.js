export default (config) => {
    const { seriesData = [], title } = config
    const data = seriesData[0].data.sort((a, b) => a.value - b.value)
    const scale = document.documentElement.clientWidth / 1920
    const legend = []
    const color = [
        '#5485FF',
        '#FF6C5F',
        '#00AEFF',
        '#8E43E7',
        '#FF8F1E',
        '#3369E7',
        '#B84592',
        '#FFC168',
        '#1CC7D0',
        '#FF4F81',
        '#2DDE98',
        '#003666',
    ]
    const len = data.length
    const per = 140 / len
    const series = data.map((item, index) => {
        const { name, value } = item
        legend.push(name)
        let result = {
            animationDelay: function(idx) {
                return idx * 20
            },
            emphasis: {
                itemStyle: { shadowColor: 'rgba(0, 0, 0, 0.2)', shadowBlur: 10 * scale },
            },
            ...item,
            type: 'pie',
            startAngle: 120,
            center: ['50%', '95%'],
            radius: [`${per * index}%`, `${per * (index + 1) - 1}%`],
            labelLine: { show: false },
            label: {
                show: true,
                position: 'inside',
                color: '#fff',
                formatter: (params) => {
                    const { value, seriesName } = params
                    if (Array.isArray(value)) {
                        return `${seriesName}`
                    }
                },
            },
            data: [
                { value: [1, value], itemStyle: { color: color[index] } },
                { value: 5, itemStyle: { color: 'transparent' } },
            ],
        }
        return result
    })

    return {
        textStyle: { color: '#666', fontSize: 14 },
        color,
        title: {
            text: title,
            textStyle: {
                color: '#222222',
                fontSize: 20,
            },
            top: 10,
            left: 10,
        },
        grid: {
            containLabel: true,
        },
        tooltip: {
            trigger: 'item',
            borderWidth: 0,
            formatter: (params) => {
                const { value, seriesName, marker } = params
                if (Array.isArray(value)) {
                    return `${marker} ${seriesName}：${value[1]}`
                }
            },
        },
        legend: {
            left: 'center',
            top: 50,
            data: legend,
        },
        series,
    }
}
