import * as echarts from 'echarts'

Date.prototype.format = function(fmt) {
    let o = {
        'M+': this.getMonth() + 1,
        'd+': this.getDate(),
        'h+': this.getHours(),
        'm+': this.getMinutes(),
        's+': this.getSeconds(),
        'q+': Math.floor((this.getMonth() + 3) / 3),
        S: this.getMilliseconds(),
    }
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length))
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(fmt))
            fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length))
    }
    return fmt
}

export default (config) => {
    const {
        seriesData = [],
        title,
        yData,
        tooltipFormatter,
        valueFormat = (value) => new Date(value).format('yyyy-MM-dd'),
    } = config
    const data = seriesData[0].data
    const scale = document.documentElement.clientWidth / 1920
    const color = [
        '#5485FF',
        '#FF6C5F',
        '#00AEFF',
        '#8E43E7',
        '#FF8F1E',
        '#3369E7',
        '#B84592',
        '#FFC168',
        '#1CC7D0',
        '#FF4F81',
        '#2DDE98',
        '#003666',
    ]
    const datas = []
    const types = []
    const typesSet = new Set()
    let min = Number.MAX_SAFE_INTEGER
    let max = Number.MIN_SAFE_INTEGER
    data.forEach((item) => {
        const { value = [], y, name, dataName = [] } = item
        value.forEach((item, index) => {
            if (typeof item !== 'number') {
                if (Number(item) == item) {
                    value[index] = Number(item)
                } else {
                    value[index] = +new Date(item)
                }
            }
        })
        if (!typesSet.has(name)) {
            typesSet.add(name)
            types.push({ name, color: color[typesSet.size - 1] })
        }
        const categoryIndex = yData.indexOf(y)
        const [s0, e0, s1, e1] = value
        const mathArr = []
        if (s0 !== undefined) mathArr.push(s0)
        if (s1 !== undefined) mathArr.push(s1)
        if (e0 !== undefined) mathArr.push(e0)
        if (e1 !== undefined) mathArr.push(e1)
        min = Math.min(min, ...mathArr)
        max = Math.max(max, ...mathArr)
        const [dataName0, dataName1] = dataName
        datas.push({
            name,
            value: [categoryIndex, y, s0, e0, s1, e1, dataName0, dataName1, name, item.color],
        })
    })

    function renderItem(params, api) {
        const categoryIndex = api.value(0)
        const start = api.coord([api.value(2), categoryIndex])
        const end = api.coord([api.value(3), categoryIndex])
        const start2 = api.coord([api.value(4), categoryIndex])
        const end2 = api.coord([api.value(5), categoryIndex])
        const height = api.size([0, 1])[1] * 0.6
        const name = api.value(8)
        const curColor = api.value(9) || types.find((item) => item.name === name).color

        const rectPractice = echarts.graphic.clipRectByRect(
            {
                x: start[0],
                y: start[1] - height / 2,
                width: end[0] - start[0],
                height: height,
            },
            {
                x: params.coordSys.x,
                y: params.coordSys.y,
                width: params.coordSys.width,
                height: params.coordSys.height,
            },
        )

        const result = {
            type: 'group',
            transition: ['shape', 'style', 'extra'],
            children: [
                {
                    type: 'rect',
                    shape: rectPractice,
                    style: { fill: curColor },
                    emphasis: { style: { fill: curColor, shadowBlur: 20 * scale, shadowColor: 'rgba(0, 0, 0, 0.2)' } },
                },
            ],
        }

        if (api.value(4) !== undefined && api.value(5) !== undefined) {
            const rectPlan = echarts.graphic.clipRectByRect(
                {
                    x: start2[0],
                    y: start2[1] - height / 2,
                    width: end2[0] - start2[0],
                    height: height,
                },
                {
                    x: params.coordSys.x,
                    y: params.coordSys.y,
                    width: params.coordSys.width,
                    height: params.coordSys.height,
                },
            )
            const item = {
                type: 'rect',
                shape: rectPlan,
                style: { fill: 'rgba(100,100,100,0.5)' },
                emphasis: {
                    style: { fill: 'rgba(100,100,100,0.5)', shadowBlur: 10 * scale, shadowColor: 'rgba(0, 0, 0, 0.2)' },
                },
            }
            if (start2[0] > start[0]) {
                result.children.push(item)
            } else if (start2[0] < start[0]) {
                result.children.unshift(item)
            } else {
                if (end2[0] - start2[0] < end[0] - start[0]) {
                    result.children.push(item)
                } else {
                    result.children.unshift(item)
                }
            }
        }

        return result
    }

    return {
        textStyle: { color: '#666', fontSize: 14 },
        color,
        title: {
            text: title,
            textStyle: {
                color: '#222222',
                fontSize: 20,
            },
            top: 10,
            left: 10,
        },
        grid: {
            containLabel: true,
            top: 60,
            left: 20,
            right: 40,
            bottom: 60,
        },

        tooltip: {
            formatter: function(params) {
                if (tooltipFormatter) return tooltipFormatter(params)
                const { value = [], name, marker } = params
                const [categoryIndex, yName, s0, e0, s1, e1, dataName0 = '实际', dataName1 = '计划'] = value

                let result = `${marker} ${name} - ${yName}<br/>\
                ${dataName0}：${valueFormat(s0)} - \
                ${valueFormat(e0)}`

                if (s1 && e1) {
                    result += `<br/>${dataName1}：${valueFormat(s1)} - \
                    ${valueFormat(e1)}<br/>`
                }

                return result
            },
        },
        dataZoom: [
            {
                type: 'slider',
                filterMode: 'weakFilter',
                showDataShadow: false,
                bottom: 20,
                labelFormatter: '',
            },
            {
                type: 'inside',
                filterMode: 'weakFilter',
            },
        ],
        xAxis: {
            min,
            max,
            type: 'value',
            scale: true,
            position: 'top',
            axisLabel: {
                formatter: valueFormat,
            },
        },
        yAxis: {
            data: yData,
            axisTick: { show: false },
        },
        series: [
            {
                type: 'custom',
                renderItem,
                encode: {
                    x: [1, 2],
                    y: 0,
                },
                data: datas,
            },
        ],
    }
}
