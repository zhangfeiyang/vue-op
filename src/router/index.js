import Vue from 'vue'
import VueRouter from 'vue-router'
import { Message } from 'element-ui'
import { getToken } from '@/utils'

Vue.use(VueRouter)

// 无权限路由列表
const noAuthViews = [{ path: 'login', title: '登录' }]
const routes = noAuthViews.map((item) => {
    const { path, title } = item
    return {
        path: '/' + path,
        name: path,
        component: () => import(`@/views/${path}`),
        meta: { title },
    }
})

const createRouter = () =>
    new VueRouter({
        mode: 'history',
        base: process.env.BASE_URL,
        routes,
    })

const router = createRouter()

// 重置路由
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher
}

// 主页路由后续会被替换，故不在createRouter中初始化，在此手动添加
router.addRoute({
    path: '*',
    component: () => import('@/views/main'),
    meta: { title: '首页' },
})

// token校验。路由权限校验不需做，动态路由已完成此功能
router.beforeEach((to, from, next) => {
    // token校验
    const path = to.path.replace('/', '')
    const doNotNeedAuth = noAuthViews.some((item) => item.path === path)
    if (doNotNeedAuth) {
        next()
    } else {
        if (!getToken()) {
            sessionStorage.setItem('preRoute', to.fullPath)
            Message.error('token失效，请重新登录')
            next('/login')
        }
        next()
    }
})

// 修改文档title
router.afterEach((to, from) => {
    document.title = to.meta?.title || '运营管理系统'
})

export default router
