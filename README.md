# 权限控制

## 页面权限

对应 views/main 页面 与 components/Menu 组件<br>
项目内的路由与菜单展示均是通过/commonApi/system/page/tree 接口获取到的数据渲染

## 按钮权限

对应 components/AuthBtn 组件，文件内有注释说明

# 状态维护

`为了减少搜索成本与理解难度，每个state的名称与对应的mution、action名称一样`

## 普通状态维护

在 store/index.js 文件中的 list 数组中添加即可，本文件没有 action

## 需保存在 sessionStorage 中的状态维护

在 store/modules/session.js 文件中的 list 数组中添加即可<br>
常见于刷新页面后还需保存的信息如登录信息，或者加快页面渲染的信息如菜单信息、参数信息

## 频繁调用的状态维护

在 store/modules/options.js 文件中的 list 数组中添加即可<br>
常见于各筛选框的下拉选项

# 路由管理

无需配置路由，由/commonApi/system/page/tree 返回数据渲染路由<br>
无权限路由在 router/index.js 文件中的 noAuthViews 数组维护

# 字体图标

可发送 iconfont 用户名给张飞阳开通权限

# 请求

http/axiosCreater.js 文件返回 axios 实例生成方法<br>
http/axios.js 文件为普通 axios 实例，发送请求可 import 此文件的实例<br>
因为关于请求所有的配置均可在 http/axiosCreater.js 与 http/axios.js 中配置，故不用单独文件维护 api，有利于项目的可阅读性

# 样式

styles/variable.scss 全局样式变量，包括覆盖 elementui 的样式变量
styles/element-reset.scss 覆盖 elementui 的样式在此维护
styles/index.scss 全局样式在此维护

# utils

utils/exe.js 此文件存放自执行的代码，如时间对象的扩展格式化方法等
utils/index.js 此文件存放需引入后调用的方法

# 页面管理

1、页面路由与文件路径严格对应（无需设置什么，系统已自动完成），方便定位
2、常规筛选框与表格加弹窗的页面可以使用 components/Page 组件快速完成开发
