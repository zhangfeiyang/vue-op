const path = require('path')
const fs = require('fs')
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')

// 获取图标列表
const getIcons = () => {
	const site = path.resolve(__dirname, './src/assets/fonts/iconfont.css')
	const iconFileContent = fs.readFileSync(site).toString()
	const reg = /icon.+?:before/g
	return iconFileContent.match(reg).map((item) => {
		return item.replace(':before', '')
	})
}

let config = {
	publicPath: '/',
	productionSourceMap: false,
	lintOnSave: false,
	devServer: {
		proxy: {
			'/dev/commonApi': {
				target: 'http://192.168.10.179:3333', // dev
				pathRewrite: { '/dev/commonApi': '/commonApi' },
				changeOrigin: true,
			},
			'/commonApi': {
				target: 'http://127.0.0.1:3333', // 本地
				changeOrigin: true,
			},
		},
		open: true,
		port: 9988,
	},
	chainWebpack: (config) => {
		// config.cache(true)
		if (process.env.BUILD_ENV !== 'txy') {
			const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
			config.plugin('cache').use(HardSourceWebpackPlugin)
		}
		// sass全局变量
		const oneOfsMap = config.module.rule('scss').oneOfs.store
		oneOfsMap.forEach((item) => {
			item.use('sass-resources-loader')
				.loader('sass-resources-loader')
				.options({
					resources: ['./src/styles/variable.scss'],
				})
				.end()
		})

		// js全局变量
		config.plugin('define').tap((args) => {
			args[0]['process.env'].ICONS_ENV = JSON.stringify(getIcons())
			return args
		})

		config.plugin('MonacoWebpackPlugin').use(MonacoWebpackPlugin)
	},

	configureWebpack: (config) => {
		// import文件时不带后缀自动尝试添加以下后缀
		config.resolve.extensions = ['.js', '.vue']
	},
}

// lib库
if (process.env.BUILD_ENV === 'lib') {
	config = { ...config, css: { extract: false } }
}

module.exports = config
